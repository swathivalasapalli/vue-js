let app = new Vue ({
    el:"#app",
    data:{
        count:0
    },
    methods:{
        onIncrement(){
            this.count++
        }
    },
    computed:{
        computedCount(){
           return this.count++
        }
    }
})