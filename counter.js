let counter = new Vue ({
    el:"#counter",
    data: {
        count:0
    },
    methods:{
        onIncrement(){
            this.count++
        },
        onDecrement(){
            this.count--
        }
    }
})