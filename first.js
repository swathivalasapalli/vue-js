let cart = new Vue({
    el:'#cart',
    data:{
        title:"Add elements to cart",
        items:[
            {text:"javascript books"}
        ]
    
    },
    methods:{
        addItem(){
            let input= document.getElementById("itemForm");
            if(input.value!== ""){
                this.items.push({text:input.value})
            input.value="";
            }
        },
        deleteItem(index) {
            this.items.splice(index, 1)
        }
    }
});