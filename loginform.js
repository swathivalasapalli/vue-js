let app = new Vue({
    el:'#app',
    data:{
      errors:[],
      name:null,
      password:null,
    },
    methods:{
      onSubmit() {
        if(this.name && this.password) return true;
        this.errors = [];
        if(!this.name) this.errors.push("Name required.");
        if(!this.password) this.errors.push("password required.");
      }
    }
  });