 
  Vue.component('product', {
    props: {
      premium: {
        type: Boolean,
        required: true
      }
    },
    template: `
     <div class="product">
          
        <div class="product-image">
          <img :src="image" />
        </div>
  
        <div class="product-info">
            <h1>{{ product }}</h1>
            <p v-if="inStock">In Stock</p>
            <p v-else>Out of Stock</p>
            <p>{{sale}}</p>
            <p>Shipping: {{ shipping }}</p>
  
            <product-details :details="details"></product-details>
             <p>{{sizes}}</p>
            <div class="color-box"
                 v-for="(variant, index) in variants" 
                 :key="variant.variantId"
                 :style="{ backgroundColor: variant.variantColor }"
                 @mouseover="updateProduct(index)"
                 >
            </div> 
  
            <button v-on:click="addToCart" 
              :disabled="!inStock"
              :class="{ disabledButton: !inStock }"
              >
            Add to cart
            </button>
            <button v-on:click="removeFromCart">Remove from cart</button>
         </div>  
         <div>
         <p v-if="!reviews.length">There are no reviews yet.</p>
         <ul v-else>
             <li v-for="(review, index) in reviews" :key="index">
               <p>{{ review.name }}</p>
               <p>Rating:{{ review.rating }}</p>
               <p>{{ review.review }}</p>
             </li>
         </ul>
     </div>
    
    <product-review @review-submitted="addReview"></product-review>
 
 </div>
    
     `,
    data() {
      return {
          product: "Cosmetics",
          brand: "Lakme",
          onSale:true,
          selectedVariant: 0,
          details: ["nourishes skin", "provides instant glow", "provides spotless skin"],
          variants: [
            {
              variantId: 1,
              variantColor: "green",
              variantImage:  "images/product.jpg",
              variantQuantity: 5    
            },
            {
              variantId: 2,
              variantColor: "blue",
              variantImage: "images/products.jpg",
              variantQuantity: 0     
            }
          ],
          reviews:[],
          cart: 0,
          sizes:["mini pack", "large", "small"],
          name:null,
          email:null
      }
    },
      methods: {
        addToCart() {
            this.$emit('add-to-cart',this.variants[this.selectedVariant].variantId)
        },
        updateProduct(index) {  
            this.selectedVariant = index
        },
        removeFromCart() {
            this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantId)
        },
        addReview(productReview) {
          this.reviews.push(productReview)
        }
      },
      computed: {
          title() {
              return this.brand + ' ' + this.product  
          },
          image(){
              return this.variants[this.selectedVariant].variantImage
          },
          inStock(){
              return this.variants[this.selectedVariant].variantQuantity
          },
          shipping() {
            if (this.premium) {
              return "Free"
            }
              return 4.99
          },
          sale(){
              if(this.onSale){
                  return this.brand +' '+this.product
              }
              return this.brand
          }
      }
  })

  Vue.component('product-details', {
    props: {
      details: {
        type: Array,
        required: true
      }
    },
    template: `
      <ul>
        <li v-for="detail in details">{{ detail }}</li>
      </ul>
    `
  })
  
  Vue.component('product-review', {
    template: `
      <form class="review-form" @submit.prevent="onSubmit">
      
        <p class="error" v-if="errors.length">
          <b>Please correct the following error(s):</b>
          <ul>
            <li v-for="error in errors">{{ error }}</li>
          </ul>
        </p>

        <p>
          <label for="name">Name:</label>
          <input id="name" v-model="name">
        </p>
        
        <p>
          <label for="review">Review:</label>      
          <textarea id="review" v-model="review"></textarea>
        </p>
        
        <p>
          <label for="rating">Rating:</label>
          <select id="rating" v-model.number="rating">
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
          </select>
        </p>

        <p>Would you recommend this product?</p>
        <label>
          Yes
          <input type="radio" value="Yes" v-model="recommend"/>
        </label>
        <label>
          No
          <input type="radio" value="No" v-model="recommend"/>
        </label>
            
        <p>
          <input type="submit" value="Submit">  
        </p>    
      
    </form>
    `,
    data() {
      return {
        name: null,
        review: null,
        rating: null,
        recommend: null,
        errors: []
      }
    },
    methods: {
      onSubmit() {
        if(this.name && this.review && this.rating && this.recommend) {
          let productReview = {
            name: this.name,
            review: this.review,
            rating: this.rating,
            recommend: this.recommend
          }
          this.$emit('review-submitted', productReview)
          this.name = null
          this.review = null
          this.rating = null
          this.recommend = null
        } else {
          if(!this.name) this.errors.push("Name required.")
          if(!this.review) this.errors.push("Review required.")
          if(!this.rating) this.errors.push("Rating required.")
          if(!this.recommend) this.errors.push("Recommendation required.")
        }
      }
    }
  })
  
  let app = new Vue({
      el: '#app',
      data: {
        premium: true,
        cart:[]
      },
      methods:{
          updateCart(id) {
              this.cart.push(id)
          },
          updateCartRemove(id){
              this.cart.pop(id)
          }
      }
  })
