let app = new Vue ({
    el:"#app",
    data:{
        columns:["title","rating"],
        ratingsinfo:[
            {title:"whitechikcs", rating:82},
            {title:"greys anatomy", rating:90},
            {title:"prison break", rating:71},
            {title:"once upon a time", rating:98}
           
        ]
    },
    methods:{
        lowestRated(){
            return this.ratingsinfo.sort((a, b) => a.rating > b.rating ? 1:-1)
        },
        highestRated(){
            return this.ratingsinfo.sort((a,b) => a.rating < b.rating ?1 : -1)
        }
    }
})
